import { Request, Response } from 'express'

import Product from '../schema/Product'

class UserController {
  public async index (req: Request, res: Response): Promise<Response> {
    const users = await Product.find()
    return res.json(users)
  }

  public async store (req: Request, res: Response): Promise<Response> {
    const product = await Product.create(req.body)
    return res.json(product)
  }
}

export default new UserController()
