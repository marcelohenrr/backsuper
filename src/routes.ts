import { Router } from 'express'
import ProductController from './controller/ProductController'

const routes = Router()

routes.get('/products', ProductController.index)
routes.post('/products', ProductController.store)

export default routes
