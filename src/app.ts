import express from 'express'
import cors from 'cors'
import mongoose from 'mongoose'

import routes from './routes'

import dotenv from 'dotenv'
dotenv.config()

class App {
    public express: express.Application

    constructor () {
      this.express = express()
      this.middlewares()
      this.database()
      this.routes()
    }

    private middlewares (): void {
      this.express.use(express.json())
      this.express.use(cors())
    }

    private database (): void {
      mongoose.connect(`mongodb://${process.env.DB_HOST}:${process.env.BD_PORT}/supermarket`,
        { useNewUrlParser: true })
    }

    private routes (): void {
      this.express.use(routes)
    }
}

export default new App().express
