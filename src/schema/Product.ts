import { Schema, model, Document } from 'mongoose'

interface ProductInterface extends Document {
  barCode: string
  price: number
}

const ProductSchema = new Schema({
  barCode: String,
  price: Number
}, {
  timestamps: true
})

export default model<ProductInterface>('Product', ProductSchema)
